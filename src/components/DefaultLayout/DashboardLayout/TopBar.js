import React from 'react';
import clsx from 'clsx';
import PropTypes from 'prop-types';
import kemnu_logo_white from '../../../../public/kemnu_full_logo.svg';
import {makeStyles} from "@mui/styles";
import {AppBar, Hidden, IconButton, Toolbar} from "@mui/material";
import MenuIcon from '@mui/icons-material/Menu';
import Image from 'next/image';
import Link from "../../../Link";

const useStyles = makeStyles(() => ({
  root: {},
  avatar: {
    width: 60,
    height: 60
  }
}));

const TopBar = ({ className, onMobileNavOpen,handleLogout, ...rest }) => {
  const classes = useStyles();

  return (
    <>
      <Hidden lgUp>
        <AppBar
          className={clsx(classes.root, className)}
          elevation={0}
          {...rest}
          style={{ background: '#fff' }}
        >
          <Toolbar>
              <IconButton
                  onClick={onMobileNavOpen}
                  size="large"
                  edge="start"
                  color="inherit"
                  aria-label="menu"
                  sx={{ mr: 2 }}
              >
                  <MenuIcon sx={{color:'#000'}}/>
              </IconButton>
            <Link href={"/"}>
              <Image
                src={kemnu_logo_white}
                alt="kemnu logo"
                height={60}
                width={'100%'}
              />
            </Link>
          </Toolbar>
        </AppBar>
      </Hidden>
    </>
  );
};

TopBar.propTypes = {
  className: PropTypes.string,
  onMobileNavOpen: PropTypes.func
};

export default TopBar;
